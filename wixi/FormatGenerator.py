
#   Copyright (C) 2007 k.remmelzwaal@planet.nl
#
#   This program is free software; you can redistribute it and/or
#   modify it under the terms of the GNU General Public License
#   as published by the Free Software Foundation; either version 2
#   of the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import re, sys, os, shutil
import SettingsDialog
import Txt2Tags

class format:

    def __init__(self, parent, contents):

	self.parent = parent

        self.cfg = SettingsDialog.Config()
        settings = self.cfg.ReadPickle()

        self.contents = contents
        self.fsenc = sys.getfilesystemencoding()
        self.encoding = settings.get('encoding')
        self.targets = settings.get('export')
	self.sl3dir = settings.get('sl3dir')
    
    def Save(self, sl3file, page, targets=None):

	if sl3file[-4:] == ".sl3":
	    sl3file = os.path.split(sl3file)[-1][:-4]

        self.exportdir = self.sl3dir + '/export/' + sl3file +'/'
        self.page = page

        if targets:
            self.targets = targets

        if not os.path.isdir(self.exportdir):
            if self.targets:
                os.mkdir(self.exportdir)

        if page and sys.platform != 'win32': 
            page = page.encode(self.fsenc,'replace')

	if page == "stylesheet":
	    self.style()
	else:
    	    save = True
    	    if 'html' in self.targets: self.html(save)
    	    if 'xhtml' in self.targets: self.xhtml(save)
    	    if 't2t' in self.targets: self.t2t(save)
    	    if 'man' in self.targets: self.man(save)
    	    if 'sgml' in self.targets: self.sgml(save)
    	    if 'tex' in self.targets: self.tex(save)
    	    if 'mgp' in self.targets: self.mgp(save)
    	    if 'lout' in self.targets: self.lout(save)
    	    if 'pm6' in self.targets: self.pm6(save)
    	    if 'moin' in self.targets: self.moin(save)

    def style(self):
	output = self.EncodeText(self.contents)
	file = open(self.exportdir+self.page+'.css','w')
	file.write(output)
	file.close()

    def t2t(self, save=False):
	contents = self.RunScript(self.contents, 't2t')
	contents = self.CopyImages(contents)
        output = contents
	output = self.EncodeText(output)
        if save == True:
            file = open(self.exportdir+self.page+'.t2t','w')
            file.write(output)
        return output

    def lout(self, save=False):
	contents = self.RunScript(self.contents, 'lout')
	contents = self.CopyImages(contents)
        output = self.Txt2Tags(contents,'lout')
	output = self.EncodeText(output)
        if save == True:
            file = open(self.exportdir+self.page+'.lout','w')
            file.write(output)
        return output

    def pm6(self, save=False):
	contents = self.RunScript(self.contents, 'pm6')
	contents = self.CopyImages(contents)
        output = self.Txt2Tags(contents,'pm6')
	output = self.EncodeText(output)
        if save == True:
            file = open(self.exportdir+self.page+'.pm6','w')
            file.write(output)
        return output

    def moin(self, save=False):
	contents = self.RunScript(self.contents, 'moin')
	contents = self.CopyImages(contents)
        output = self.Txt2Tags(contents,'moin')
	output = self.EncodeText(output)
        if save == True:
            file = open(self.exportdir+self.page+'.moin','w')
            file.write(output)
        return output

    def mgp(self, save=False):
	contents = self.RunScript(self.contents, 'mgp')
	contents = self.CopyImages(contents)
        output = self.Txt2Tags(contents,'mgp')
	output = self.EncodeText(output)
        if save == True:
            file = open(self.exportdir+self.page+'.mgp','w')
            file.write(output)
        return output

    def tex(self, save=False):
	contents = self.RunScript(self.contents, 'tex')
	contents = self.CopyImages(contents)
        output = self.Txt2Tags(contents,'tex')
	output = self.EncodeText(output)
        if save == True:
            file = open(self.exportdir+self.page+'.tex','w')
            file.write(output)
        return output

    def sgml(self, save=False):
	contents = self.RunScript(self.contents, 'sgml')
        contents = self.CopyImages(contents)
        contents = self.RelinkUrls(contents,'sgml')
        output = self.Txt2Tags(contents,'sgml')
        output = self.EncodeText(output)
        if save:
            file = open(self.exportdir+self.page+'.sgml','w')
            file.write(output)
        return output

    def man(self, save=False):
	contents = self.RunScript(self.contents, 'man')
	contents = self.CopyImages(contents)
        output = self.Txt2Tags(contents,'man')
	output = self.EncodeText(output)
        if save == True:
            file = open(self.exportdir+self.page+'.man','w')
            file.write(output)
        return output

    def xhtml(self, save=False):
	contents = self.RunScript(self.contents, 'xhtml')
	contents = self.CopyImages(contents)
        contents = self.RelinkUrls(contents,'xhtml')
        output = self.Txt2Tags(contents,'xhtml')
        output = self.EncodeText(output)
        output = self.SetImageSize('xhtml', output)
        if save:
            file = open(self.exportdir+self.page+'.xhtml','w')
            file.write(output)
        return output

    def html(self, save=False):
	contents = self.RunScript(self.contents, 'html')
	contents = self.IncludePages(contents)
        if save:
	    contents = self.CopyImages(contents)
            output = self.Txt2Tags(contents,'html')
	    output = self.RelinkUrls(output,'html')
            output = self.SetImageSize('html', output)
	    output = self.EncodeText(output)
            file = open(self.exportdir+self.page+'.html','w')
            file.write(output)
        else:
            output = self.Txt2Tags(contents,'html')
            output = self.SetImageSize('html', output)
	    output = self.NonExists(output)
	    output = self.Highlight(output)
	    output = self.SetEditLinks(output)
	    output = self.ResizeHeaders(output)

        return output

    def RunScript(self, text, target):
    
        try: scriptfile = self.parent.parent.parent.sl3db
        except: scriptfile = self.parent.sl3db

        scriptfile = os.path.basename(scriptfile)[:-4] + ".py"    
	scriptdir = self.sl3dir + '/plugins/'
	    
	if os.path.isfile(scriptdir+scriptfile):
            code = open(scriptdir+scriptfile)
	    code = code.read()
            code = compile(code, 'wixi','exec')

	    text = self.parent.RunScript(text, code)

	return text

    def PutHlMarks(self, mark, text):
	'''Encloses the highlighted text and ignore all text wich are inside links.'''

	def AvoidList(text):
	    '''Generate a list of all link locations'''
	    locs = []

	    # look for link patterns or links wich are cut.
	    patt1 = re.compile('\[.*?]', re.MULTILINE)
	    patt2 = re.compile('^.*?\]', re.MULTILINE)

	    iter1 = patt1.finditer(text)
	    iter2 = patt2.finditer(text)

	    for m in iter1:locs.append(m.span())
	    for m in iter2:locs.append(m.span())

	    return locs

	patt, hl_text = re.compile(mark, re.IGNORECASE), ""

	while 1:

	    found = patt.search(text)
	    if not found:
		hl_text += text
		break

	    loc, avoid = found.span(), AvoidList(text)
	    start, end = loc[0], loc[1]
	    
	    ignore = False
	    for a in avoid:				# ignore the text if the location of the first
		if start > a[0] and start < a[1]:	# letter is between a link or image.
		    ignore = True
		    break
	    
	    if ignore: hl_text += text[:end]	# cutting the text.
	    else: hl_text += text[:start] + '{' + text[start:end] + '}' # cutting and adding marks.

	    text = text[end:]	# cut the text and repeat the process.

	return hl_text

    def ResizeHeaders(self, html):
	'''Just to make it look good in the wx.html browser'''
	html = html.replace("<H1>","<H3>")
	html = html.replace("</H1>","</H3>")
	
	html = html.replace("<H2>","<H3>")
	html = html.replace("</H2>","</H3>")
	return html

    def SetEditLinks(self, html):
    
	patt = re.compile(r'(\<H[0-9]>)(.*?)(</H[0-9]>)')
	headers = re.findall(patt, html)
	
	if len(headers) > 4:
	
	    for header in headers:
		if not "</FONT>" in header[1]:
		    head = header[0] + header[1] + header[2]
		    link = '<SMALL><I><A HREF="edit:%s"> edit </A></I></SMALL>' % (header[1])
		    html = html.replace(head ,header[0] + header[1] + link + header[2])

	patt = re.compile(r'(\<IMG.*?SRC=")(.*?)(".*?\>)')
	images, img_list = re.findall(patt, html), []
	for img in images:

	    if unicode(img[1]) in img_list:
		continue

	    elif "dia" in img[1]:
		image = img[0] + img[1] + img[2]
		link = '<SMALL><I><A HREF="image:%s"> edit </A></I></SMALL>' % (img[1])
		html = html.replace(image ,image + link)
	    else:
		image = img[0] + img[1] + img[2]
		link = '<SMALL><I>%s</I></SMALL>' % (img[1])
		html = html.replace(image ,image + link)

	    img_list.append(img[1])
	
	return html

    def Highlight(self, html):
	patt = re.compile(r'{(.*?)}')
	html = patt.sub(r'<FONT COLOR="YELLOWGREEN"><B><I>\1</I></B></FONT>', html)
	return html

    def NonExists(self, html):
        patt = re.compile(r'\<A HREF=".*?"\>.*?</A>')
	links = re.findall(patt, html)

	for link in links:
	    link_patt = re.compile(r'HREF=".*?"')
	    loc = link_patt.search(link).span()

	    begin, end = loc[0], loc[1]
	    page = link[9:(end-1)]
	    	    
	    href_patt = re.compile(r'<A HREF="%s">(.*?)</A>' % (page))
	    if "#toc" in page: color = None
	    elif not self.parent.parent.parent.sql.PageExist(page):
	    
		if ":" in page: color = "STEELBLUE"
		else: color = "TOMATO"
		
		html = href_patt.sub(r'<A HREF="%s">\1</A><FONT COLOR="%s">*</FONT>' % (page, color), html)
	    else:
		html = href_patt.sub(r'<A HREF="%s">\1</A>' % (page), html)

	return html

    def IncludePages(self, contents):
	patt_include = re.compile("^%!wixi-include:.*?$", re.MULTILINE) # find wixi-images tags

	fetch = patt_include.findall(contents)
	
	for include in fetch:
	
	    include_line = include
	    include = include[15:]
	    include = include.replace(" ","")
	    
	    try: 
		if self.parent.parent.parent.sql.PageExist(include):
		    page_id = self.parent.parent.parent.sql.GetPageId(include)
		    page_data = self.parent.parent.parent.sql.GetPageData(page_id)
		    contents = contents.replace(include_line, page_data.get("contents"))
	    
    	    except: 
		if self.parent.sql.PageExist(include):
		    page_id = self.parent.sql.GetPageId(include)
		    page_data = self.parent.sql.GetPageData(page_id)
		    contents = contents.replace(include_line, page_data.get("contents"))
	
	return contents
		
    def SetImageSize(self, target, html):
    
        patt_imgtag = re.compile("^%!wixi-images:.*?$", re.MULTILINE) # find wixi-images tags
        patt_split = re.compile("\s*,\s*") # splitting by comma
        patt_rmspaces = re.compile("([\w_,.+%$#@!?+~/-]+)") # getting the whole words without spaces

        fetch = patt_imgtag.findall(self.contents)

        if len(fetch) == 0: return html

        for imgs in fetch:

            imgs = imgs[14:]
            imgs = patt_split.split(imgs)
            
            for img in imgs:
        	img = patt_rmspaces.findall(img)

        	if len(img) == 3:
        	
        	    img_name, img_width, img_height = img[0], img[1], img[2]
        	
        	    if target == "html":

    			patt_img = re.compile(r'\<IMG(.*?)%s" (.*?)\>' % (img_name))
    			html = patt_img.sub(r'<IMG\1%s" WIDTH="%s" HEIGHT="%s" \2>' % (img_name, img_width, img_height), html)
    		
    		    if target == "xhtml":
		    
    			patt_img = re.compile(r'\<img(.*?)%s" (.*?)\>' % (img_name))
    			html = patt_img.sub(r'<img\1%s" width="%s" height="%s" \2>' % (img_name, img_width, img_height), html)
        	    
	return html


    def RelinkUrls(self, html, ext):
    
	link_patt = re.compile(r'<A HREF="(.*?)">(.*?)</A>')
        links = re.findall(link_patt, html)

        for link, name in links:
    	    if ":" not in link:
    		if "." not in link:
    		    if "#toc" not in link:
    			link_patt = re.compile(r'<A HREF="%s">(.*?)</A>' % (link))
    			html = link_patt.sub(r'<A HREF="%s.%s">\1</A>' % (link, ext), html)

        return html

    def CopyImages(self, contents):

        img_patt = r'\[([\w_,.+%$#@!?+~/-]+\.(png|jpe?g|gif|eps|bmp))\]'
        imgs = re.findall(img_patt, contents)

	if imgs and not os.path.isdir(self.exportdir+'images'):
	    os.mkdir(self.exportdir+'images')

        for img, ext in imgs:
        
            new_img = os.path.basename(img)

	    try: shutil.copyfile(img, self.exportdir +'images/'+new_img)
	    except: continue

            old_tag = "["+img+"]"
            new_tag = "[images/"+os.path.basename(img)+"]"
            contents = contents.replace(old_tag, new_tag)

        return contents

    def DecodeText(self, text):
        if isinstance(text, str):
            text = text.decode(self.encoding)
        return text

    def EncodeText(self, text):
        if isinstance(text, unicode):
            text = text.encode(self.encoding)
        return text

    def Txt2Tags(self, text, target):
    
	if "%%toc" in text: toc = 1
	else: toc = 0

        text = self.EncodeText(text)
        text = [''] + text.splitlines()

    	source = Txt2Tags.process_source_file(contents=text)
	source[0].update({'toc': toc, 'style':['stylesheet.css'],'target':target, 'encoding':self.encoding})

	try: source = Txt2Tags.convert_this_files([source])[0]
        except: source = ["Error: Could not convert this document."]

        output = ''
        for r in source:
            output += self.DecodeText(r) + '\n'

        return output
