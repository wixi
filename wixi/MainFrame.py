
#   Copyright (C) 2007 k.remmelzwaal@planet.nl
#
#   This program is free software; you can redistribute it and/or
#   modify it under the terms of the GNU General Public License
#   as published by the Free Software Foundation; either version 2
#   of the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import wx, sys, os, re, webbrowser, shutil, difflib
import wx.html as html
import wx.stc as stc
import wx.lib.imagebrowser as imagebrowser
import wx.lib.mixins.listctrl as listmix

import FormatGenerator
import SettingsDialog
import SQLiteCon
import QuickrefDialog
import CalendarDialog
import AboutDialog
import Desktop

stylesheet = '''
/* Note that css is only supported by exported html/xhtml pages. */

body {
  position: absolute;
  left: 20px;
  font-family: arial, serif;
  font-size: 14px;
}

p dl {
  max-width: 650px;
}

code {
  background-color: #eee;
  border: 1px dotted #ddd;
}

pre {
  background-color: #eee;
  margin-left: 2em;
  margin-right: 2em;
  padding: 8px;
  border: 1px outset gray;
  font-family: courier, courier new;
  font-size: 95%;
}

table {
  font-size: 100%; /* Needed to inherit the body font size*/
  border-color: #ccc;
}

th {
  background-color: #ddd;
  padding: .5em 0;
  border-color: #ccc;
  border-style: solid;
}

td {
  border-style: none;
  padding: .2em .5em;
}

a {
  text-decoration: none;
}

a:link {
  color: blue;
  font-weight: bold;
}

a:hover {
  text-decoration: underline;
}

a:visited {
  color: #66f;
}

'''

class HtmlPanel(html.HtmlWindow):

    def __init__(self, parent, id):
        html.HtmlWindow.__init__(self, parent, id, style=wx.NO_FULL_REPAINT_ON_RESIZE)

	self.parent = parent
	self.online_mode = False

        self.SetStandardFonts(self.GetConfigFontSize())
    
    def SetFontSize(self, size):
	'''Set the font size for the editor'''
	self.SetStandardFonts(size=size)

    def GetConfigFontSize(self):
	'''Get the font size from the config file'''
	config = SettingsDialog.Config() 
        fontsize = config.ReadPickle().get('htmlfontsize')
	return fontsize

    def LoadUrl(self, url):
	self.LoadPage(url)
	self.online_mode = True

    def OnLinkClicked(self, linkinfo):
        link = linkinfo.GetHref()
        
	if self.online_mode:
	    if link[-5:] == ".html":
		self.LoadPage(link)
	    else:
		self.online_mode = False
		self.HistoryClear()
	elif link.split(':')[0] in ('http','https','ftp'): webbrowser.open(link)
	elif link[:5] == 'edit:':
	    self.parent.SetSelection(1)
	    self.parent.edit.SetCursorPos(link[5:])
	elif link[:6] == 'image:':
	    wx.Execute("kivio kivio/%s" % (link[6:-4] + ".flw"))
	    #Desktop.open(link[6:])
        elif link[:7] == 'file://':
	    link = link.replace("%20"," ")
            if ':' in link[7:]: Desktop.open(link[7:])  # format in windows file://c:/readme.txt
            else: Desktop.open(link[7:])                # format in posix file:///home/nix/readme.txt
        elif "#toc" in link:
	    super(HtmlPanel, self).OnLinkClicked(linkinfo)
        elif sql.ValidPage(link):
            page_id = sql.GetPageId(link)
            data = sql.GetPageData(page_id)
            self.parent.SetPage(page_id, data.get('page'))
        else:
            dlg = wx.TextEntryDialog(self, 'page name','new page',link)
            if dlg.ShowModal() == wx.ID_OK:
                name = dlg.GetValue()
                name = name.replace(' ','_')
                if name:
                    if not sql.PageExist(name):
                        self.parent.SetPage(None, name)
                    else:
                        dlg = wx.MessageDialog(self,'Page %s already exist!' % (name),'New Page', wx.ICON_INFORMATION)
                        dlg.ShowModal()
                        dlg.Destroy()

class Styled(stc.StyledTextCtrl):

     def __init__(self, parent, ID):
        self.stc =stc.StyledTextCtrl.__init__(self, parent, ID)


class EditPanel(wx.Panel):

    def __init__(self, parent):
        wx.Panel.__init__(self, parent, -1)

        self.editor = Styled(self, -1)
	self.editor.SetMarginType(0, stc.STC_MARGIN_NUMBER)
        self.editor.SetMarginWidth(0, 25)
	self.editor.SetTabWidth(4)

	self.font = "Courier New"
	self.SetFontSize(self.GetConfigFontSize())
	self.SetFont(self.font)
	
	self.SetWrapMode()

        self.comment = wx.TextCtrl(self, -1, '',size=(300, -1))

        box2 = wx.BoxSizer(wx.HORIZONTAL)
        box2.Add(wx.StaticText(self, -1, "comment:"))
        box2.Add(self.comment, 1, wx.EXPAND)
        
        box = wx.BoxSizer(wx.VERTICAL)
        box.Add(self.editor, 1, wx.EXPAND)
        box.Add(box2, 0, wx.EXPAND)
	
	self.Bind(stc.EVT_STC_MODIFIED, self.Modified)

        self.SetSizer(box)
        self.SetAutoLayout(1)

	self.modified = 0

    def SetCursorPos(self, header):
    
	pos = self.editor.FindText(0, len(self.GetText()), "= " + header)
	if pos == -1: pos = self.editor.FindText(0, len(self.GetText()), "=" + header)
		
	self.editor.SetSTCFocus(1)
	self.editor.SetCurrentPos(pos)
	self.editor.SetSelection(pos ,pos)
	
	self.editor.ScrollToLine(self.editor.GetCurrentLine())
	
    def SetWrapMode(self):
	config = SettingsDialog.Config() 
        wrap = config.ReadPickle().get('editwrap')

        if wrap == 1: self.editor.SetWrapMode(1)
    	else: self.editor.SetWrapMode(0)

    def GetConfigFontSize(self):
	'''Get the font size from the config file'''
	config = SettingsDialog.Config() 
        fontsize = config.ReadPickle().get('editfontsize')
	return fontsize

    def SetFontSize(self, size):
	'''Set the font size of the editor'''
	self.editor.StyleSetSpec(stc.STC_STYLE_DEFAULT, "size:"+str(size)+",face:"+self.font)
	self.fontsize = size

    def SetFont(self, font):
	'''Set the font of the editor'''
	size = str(self.fontsize)
	self.editor.StyleSetSpec(stc.STC_STYLE_DEFAULT, "size:"+size+",face:"+font)
	self.font = font

    def Modified(self, ev):
	'''After changing the text, add plus 1'''
	self.modified += 1
    	
    def AppendText(self, text):
	'''Append text to the end of text'''
	self.editor.AppendText(text)

    def AddText(self, text):
	'''Add text to current cursor position'''
	self.editor.AddText(text)

    def GetText(self):
	'''Get text from the editor'''
	return self.editor.GetText()

    def SetText(self, text):
	'''Set text to the editor'''
	self.editor.SetText(text)


class HistoryList(wx.ListCtrl, listmix.ListCtrlAutoWidthMixin):
    def __init__(self, parent, ID, pos=wx.DefaultPosition, size=wx.DefaultSize, style=0):
        
        wx.ListCtrl.__init__(self, parent, ID, pos, size, style = wx.LC_REPORT | wx.LC_SINGLE_SEL)
        
        listmix.ListCtrlAutoWidthMixin.__init__(self)
        
        self.InsertColumn(0, "date", width = 170)
        self.InsertColumn(1, "log entry")
        
    def Set(self, evlist):
        """ Set event list, which clears the previous list """
        
        self.DeleteAllItems()
        
        for sdate, scomment in evlist:
            index = self.InsertStringItem(sys.maxint, sdate)
            self.SetStringItem(index, 1, scomment)
        
    def GetSelection(self):
        """ Returns single selection of the log entry """
        return self.GetFirstSelected()


class HistoryPanel(wx.Panel):
    
    def __init__(self, parent):
	wx.Panel.__init__(self, parent, -1)

	self.history = HistoryList(self, -1, (100,100), (100,100))

	box = wx.BoxSizer(wx.VERTICAL)
	box.Add(self.history, 1, wx.EXPAND)

	self.SetSizer(box)
	self.SetAutoLayout(1)

    def SetList(self, evlist):

	self.historylist = evlist

	hlist = []
	for e in evlist:
	    hlist.append( (e[1], e[2]) )

	self.history.Set(hlist)

    def GetSelection(self):
	select = self.history.GetSelection()
	pg_id = self.historylist[select]
	return pg_id


class DiffPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, -1)

        self.choices = []

        self.ch1 = wx.Choice(self, -1, (100, 50), choices = self.choices )
        self.ch2 = wx.Choice(self, -1, (200, 50), choices = self.choices )
	self.difftext = wx.TextCtrl(self, -1, '', style=wx.TE_MULTILINE)
	self.difftext.SetEditable(False)

        box2 = wx.BoxSizer(wx.HORIZONTAL)
        box2.Add(self.ch1, wx.EXPAND)
        box2.Add(self.ch2, wx.EXPAND)

        box = wx.BoxSizer(wx.VERTICAL)
        box.Add(box2, 0, wx.EXPAND)
        box.Add(self.difftext, 1, wx.EXPAND)

        self.Bind(wx.EVT_CHOICE, self.EvtChoice1, self.ch1)
        self.Bind(wx.EVT_CHOICE, self.EvtChoice2, self.ch2)

        self.SetSizer(box)
        self.SetAutoLayout(1)

    def GetPage(self, ch):
        page_id = self.historylist[ch][0]
        page_data = sql.GetPageData(page_id)
        page_contents = page_data.get('contents')

	return page_contents

    def Compare(self, ch1, ch2):

        page1 = self.GetPage(ch1).splitlines()
        page2 = self.GetPage(ch2).splitlines()

	result, text1, text2 = "", [], []

	for l in page1: text1.append(l+'\n')
	for l in page2: text2.append(l+'\n')
	
	diff = difflib.unified_diff(text2, text1, n=False)

	for l in diff: result += l

	if len(result) == 0: result = "No differences found."
	self.difftext.SetValue(result)

	c = 0
	for line in result.splitlines():
	    if line[0] == "+": self.difftext.SetStyle(c, c+1, wx.TextAttr("WHITE","BLUE"))
	    if line[0] == "-": self.difftext.SetStyle(c, c+1, wx.TextAttr("WHITE","RED"))
	    c += len(line)+1
	
    def EvtChoice1(self, event):
        ch1 = self.ch1.GetSelection()
        ch2 = self.ch2.GetSelection()

        if ch1 != -1 and ch2 != -1:
            self.Compare(ch1, ch2)

    def EvtChoice2(self, event):
        ch1 = self.ch1.GetSelection()
        ch2 = self.ch2.GetSelection()

        if ch1 != -1 and ch2 != -1:
            self.Compare(ch1, ch2)

    def SetList(self, history):
	self.historylist = history
	self.difftext.SetValue("")
	self.ch1.Clear()
	self.ch2.Clear()
	
	choices = []
	for e in history:
	    self.ch1.Append(e[1])
	    self.ch2.Append(e[1])


class NotebookRight(wx.Notebook):

    def __init__(self, parent, id):
        wx.Notebook.__init__(self, parent, id, size=(21,21))

	self.parent = parent

        self.html = HtmlPanel(self, -1)
        self.edit = EditPanel(self)
        self.history = HistoryPanel(self)
	self.compare = DiffPanel(self)
	
        self.AddPanels()
        self.SetSelection(0)

	self.Bind(wx.EVT_NOTEBOOK_PAGE_CHANGED, self.OnTabChanged)

    def AddPanels(self):
        viewmode = self.parent.parent.options.get('viewmode')

        self.AddPage(self.html, "viewer")
        if not viewmode:
            self.AddPage(self.edit, "editor")
            self.AddPage(self.history, "history")
	    self.AddPage(self.compare, "compare")

    def RunScript(self, text, code):
	exec(code)
	return text

    def SetPage(self, pg_id, pg_title):
	self.parent.SetPage(pg_id, pg_title)

    def GenHtmlCode(self, text, highlight=None):

	gen = FormatGenerator.format(self, text)
	if highlight:
	    gen.contents = gen.PutHlMarks(highlight, text)
	html = gen.html()
	html = gen.DecodeText(html)

	self.html.SetPage(html)

    def OnTabChanged(self, event):
	'''update the html page when changing tabs'''
    
        prev = event.GetOldSelection()
        cur = self.GetSelection()

        if prev == 1:
	    # when from editor tab, update html page
        
            text = self.edit.GetText()
	    self.GenHtmlCode(text) 

        event.Skip()


class TreePanel(wx.TreeCtrl):

    def __init__(self, parent, id):

        wx.TreeCtrl.__init__(self, parent, id)

        isz = (16,16)
        self.il = wx.ImageList(isz[0], isz[1])

	IconSize = "16x16"
        self.folder           = self.il.Add(wx.Image(IconDir+IconSize+'/category_closed.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap())
        self.folder_open      = self.il.Add(wx.Image(IconDir+IconSize+'/category_open.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap())
        self.folder_grey      = self.il.Add(wx.Image(IconDir+IconSize+'/category_special_closed.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap())
        self.folder_open_grey = self.il.Add(wx.Image(IconDir+IconSize+'/category_special_open.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap())
	self.page             = self.il.Add(wx.Image(IconDir+IconSize+'/page.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap())
	self.page_special     = self.il.Add(wx.Image(IconDir+IconSize+'/page_special.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap())

        self.SetImageList(self.il)

        self.Bind(wx.EVT_TREE_ITEM_EXPANDING, self.OnItemExpanding, id=self.GetId())
        self.Bind(wx.EVT_TREE_ITEM_COLLAPSED, self.OnItemCollapsed, id=self.GetId())

    def AddIndex(self):

        self.DeleteAllItems()

        data = sql.GetCategoryNames('/')

        self.root = self.AddRoot('Index')
        self.SetPyData(self.root, data)
        self.SetItemImage(self.root, self.folder, wx.TreeItemIcon_Normal)
        self.SetItemImage(self.root, self.folder_open, wx.TreeItemIcon_Expanded)

        if data: self.SetItemHasChildren(self.root, True)

        self.SetFont(self.root)
        self.Collapse(self.root)

    def SetFont(self, item):
    
	fontsize = self.GetConfigFontSize()
        font = wx.Font(fontsize, wx.DEFAULT, wx.NORMAL, wx.NORMAL, False, "Sans")
        self.SetItemFont(item, font)

    def GetConfigFontSize(self):
	'''Get the font size from the config file'''
	config = SettingsDialog.Config() 
        fontsize = config.ReadPickle().get('navfontsize')
	return fontsize

    def Reset(self):
        self.Collapse(self.root)

    def RefreshAppend(self, child, cat_ev, path):

        name = path.split('/')[-1]
        data = self.GetPyData(child)
        data = [{'type':'category', 'id':cat_ev, 'name':name, 'path':path}] + data

        self.SetPyData(child, data)
        self.CollapseAndReset(child)
        self.SetItemHasChildren(child, True)

    def RefreshDelete(self, child, itype, name):

        parent = self.GetItemParent(child)
        data = self.GetPyData(parent)

        x = 0
        for item in data:
            if item.get('type') == itype:
                if item.get('name') == name:
                    break
            x += 1

        data.pop(x)
        self.Delete(child)
        self.SetPyData(parent, data)

    def RefreshRename(self, itype ,old_name, new_name):

        child = self.GetSelection()
        parent = self.GetItemParent(child)
        data = self.GetPyData(parent)

        x = 0
        for item in data:
            if item.get('type') == itype:
                if item.get('name') == old_name:
                    if item.get('type') == 'category':
                        new_path = item.get('path')
                        new_path = new_path[:-len(old_name)] + new_name
                        item.update({'path':new_path})
                    item.update({'name':new_name})
                    data[x] = item
                    break
            x += 1

        self.SetPyData(parent, data)
        self.CollapseAndReset(parent)

    def OnItemExpanding(self, event):

        parent = event.GetItem()
        itemdata = self.GetPyData(parent)

        for item in itemdata:

            child = self.AppendItem(parent, item.get('name'))
	    pages = []

            if item.get('type') == 'page':
		if item.get('name') in special_pages: self.SetItemImage(child, self.page_special, wx.TreeItemIcon_Normal)
		else: self.SetItemImage(child, self.page, wx.TreeItemIcon_Normal)
		self.SetItemHasChildren(child, False)
            
	    else:
                if item.get('name') == 'AllPages': pages = sql.ShowAllPages()
		elif item.get('name') == 'RemovedPages': pages = sql.ShowRemovedPages()
		elif item.get('name') in ('Events','Special'):
		    pages += [ {'type':'page','id':'0','name':'Changelog'} ]
		    pages += [ {'type':'page','id':'0','name':'AllPages'} ]
		    pages += sql.GetPageNames(item.get('name'))
		else: pages = sql.GetPageNames(item.get('name'))

                data = sql.GetCategoryNames(item.get('path'))
                data.extend(pages)

                self.SetPyData(child, data)

                if item.get('name') in ('Special','Events','AllPages','RemovedPages'):
		    self.SetItemImage(child, self.folder_grey, wx.TreeItemIcon_Normal)
		    self.SetItemImage(child, self.folder_open_grey, wx.TreeItemIcon_Expanded)
		else:
		    self.SetItemImage(child, self.folder, wx.TreeItemIcon_Normal)
		    self.SetItemImage(child, self.folder_open, wx.TreeItemIcon_Expanded)
                
		if data: self.SetItemHasChildren(child, True)

            self.SetFont(child)

    def OnItemCollapsed(self, event):

        item = event.GetItem()
        self.CollapseAndReset(item)

    def GetItemInfo(self, item):

        itemtext = self.GetItemText(item)
        itemlist = self.GetItemParent(item)
        itemlist = self.GetPyData(itemlist)
	
	for item in itemlist:
	    if item.get('type') == 'page':
		name = item.get('name')
		if name == itemtext:
		    return item

	for item in itemlist:
	    if item.get('type') == 'category':
		name = item.get('name')
		if name == itemtext:
		    return item



class SearchPanel(wx.Panel):

    def __init__(self, parent):
        wx.Panel.__init__(self, parent, -1)

	try: self.findtext = wx.SearchCtrl(self, -1, size=(200,-1), style=wx.TE_PROCESS_ENTER)
	except: self.findtext = wx.TextCtrl(self, -1, "Find", size=(200,-1))
	
	self.foundlist = wx.ListBox(self, -1, (100,100), (100,100), [], wx.LB_SINGLE)
	
	box = wx.BoxSizer(wx.VERTICAL)
	box.Add(self.findtext, 0, wx.EXPAND)
	box.Add(self.foundlist, 1, wx.EXPAND)

	self.SetSizer(box)
	self.SetAutoLayout(1)

	self.Bind(wx.EVT_TEXT, self.Find, self.findtext)

    def Find(self, ev):
	text = self.findtext.GetValue()
	
	if len(text) > 1: self.SetList(sql.Search(text))
	else: self.foundlist.Set([])
    
    def SetList(self, evlist):

	self.found = evlist

	found = []
	for e in evlist:
	    found.append(e[1])

	self.foundlist.Set(found)

    def GetSelection(self):
	select = self.foundlist.GetSelection()
	pg_id = self.found[select]
	return pg_id


class NotebookLeft(wx.Notebook):

    def __init__(self, parent, id):
        wx.Notebook.__init__(self, parent, id, size=(21,21))

        self.tree = TreePanel(self, -1)
        self.AddPage(self.tree, "categories")

        self.search = SearchPanel(self)
        self.AddPage(self.search, "search")

        self.SetSelection(0)
    

class Splitter(wx.SplitterWindow):
    def __init__(self, parent, id):
        wx.SplitterWindow.__init__(self, parent, id, style = wx.SP_LIVE_UPDATE)
	sty = wx.BORDER_SUNKEN

	self.parent = parent
	
	self.pl = NotebookLeft(self, -1,)
	self.pr = NotebookRight(self, -1,)
	self.SetMinimumPaneSize(0)

        sashpos = parent.GetWindowSettings()[1]
	nav = parent.options.get('navpanel')
	if nav == "right": self.SplitVertically(self.pr, self.pl, -sashpos)
	else: self.SplitVertically(self.pl, self.pr, sashpos)
	if nav == "off": self.Unsplit(self.pl)

    def SetPage(self, pg_id, pg_title):
	self.parent.SetPage(pg_id, pg_title)


class MainFrame(wx.Frame):

    def __init__(self, parent, id, title, options):
    
        if options.get('geometry'): wsize = options.get('geometry')
        else: wsize = self.GetWindowSettings()[0]
        
	wx.Frame.__init__(self, parent, id, title, size=wsize)
	
	global IconDir
	IconDir = options.get('icondir')
	self.IconDir = IconDir
	self.options = options

        self.LoadMenu()
        self.LoadIcons()

	self.CreateStatusBar()

        self.splitter = Splitter(self, -1)
	self.CreateDirLayout()
	self.Sl3Connect(self.options.get('wiki'))

        box = wx.BoxSizer(wx.VERTICAL)
        box.Add(self.splitter, 1, wx.ALL|wx.GROW, 1)

	self.Bind(wx.EVT_TOOL, self.OnClose, id=105)
	self.Bind(wx.EVT_TOOL, self.ShowSettingsDlg, id=400)
	self.Bind(wx.EVT_TOOL, self.SavePage, id=100)
	self.Bind(wx.EVT_TOOL, self.NewPage, id=202)
	self.Bind(wx.EVT_TOOL, self.GoHome, id=200)
	self.Bind(wx.EVT_TOOL, self.OpenSl3File, id=102)
	self.Bind(wx.EVT_TOOL, self.NewSl3File, id=201)
	self.Bind(wx.EVT_TOOL, self.GoForward, id=230)
	self.Bind(wx.EVT_TOOL, self.GoBack, id=220)
	self.Bind(wx.EVT_TOOL, self.ImageManager, id=500)
	self.Bind(wx.EVT_TOOL, self.QuickRef, id=301)
	self.Bind(wx.EVT_TOOL, self.LinkedPages, id=402)
	self.Bind(wx.EVT_TOOL, self.ImportFiles, id=103)
	self.Bind(wx.EVT_TOOL, self.ShowCalendarDlg, id=403)
	self.Bind(wx.EVT_TOOL, self.LoadDocumentation, id=302)
	self.Bind(wx.EVT_TOOL, self.ShowAboutDlg, id=303)
	self.Bind(wx.EVT_TOOL, self.DumpAllPages, id=404)
	self.Bind(wx.EVT_TOOL, self.StyleSheet, id=405)

	self.Bind(wx.EVT_CLOSE, self.OnClose)

	self.splitter.pl.tree.Bind(wx.EVT_RIGHT_DOWN, self.OnRightClick)
	self.splitter.pl.tree.Bind(wx.EVT_LEFT_DCLICK, self.OnLeftClick)
	self.splitter.pr.history.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self.OnHistoryClick)
	self.splitter.pl.search.Bind(wx.EVT_LISTBOX_DCLICK, self.OnSearchDClick)

        self.SetSizer(box)
        self.SetAutoLayout(1)
        self.Show(1)
    
    def GetWindowSettings(self):
        settings = SettingsDialog.Config()
        config = settings.ReadPickle()
        winset = config.get('winset')
        
        if not winset:
            return ((850,550), 220)
        return (winset[0], winset[1])
    
    def SaveWindowSettings(self):
        settings = SettingsDialog.Config()
        config = settings.ReadPickle()
        config.update({"winset": (self.GetSize(), self.splitter.GetSashPosition())})
        settings.WritePickle(config)
    
    def LoadMenu(self):

	SubMenuNew = wx.Menu()                                                                                                      
	SubMenuNew.Append(202, "Wiki page","Create a new page.")
	SubMenuNew.Append(201, "Wiki database","Create a new wiki database.")
	
        MenuWiki = wx.Menu()                                                                                                            
        MenuWiki.AppendMenu(101, "New", SubMenuNew)
	MenuWiki.Append(102, "Open wiki", "Open a wiki database file.")
        MenuWiki.AppendSeparator()
        MenuWiki.Append(103, "Import files","Importing t2t files.")
        MenuWiki.Append(400, "Settings","Configuration menu.")
        MenuWiki.AppendSeparator() 
	MenuWiki.Append(105, "Exit", "Exit program.")                                                                                           
        MenuTools = wx.Menu()
	MenuTools.Append(500, "Image browser","Copy images to images directory.")
	MenuTools.Append(402, "Linked pages", "Show a list of pages that are linked to this page.")
	MenuTools.Append(404, "Export all pages","Exporting all pages in the database.")
	MenuTools.Append(403, "Calendar", "Calendar dialog.")
	MenuTools.Append(405, "Stylesheet page", "Make your exported html pages look better with stylesheet.")

	MenuHelp = wx.Menu()
	MenuHelp.Append(301, "Quick Reference","Shows a quick reference dialog.")
	MenuHelp.Append(302, "Documentation","Load online documentation.")
	MenuHelp.Append(303, "About","About dialog.")

        menuBar = wx.MenuBar()
        menuBar.Append(MenuWiki,"&Wiki")
        menuBar.Append(MenuTools, "&Tools")
	menuBar.Append(MenuHelp, "&Help")
	
	self.SetMenuBar(menuBar)

    def LoadIcons(self):

	IconSize = "16x16"
	if '__WXGTK__' in wx.PlatformInfo: IconSize = self.options.get('iconsize')

	png_save = wx.Image(IconDir+IconSize+'/save_page.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap()
	png_home = wx.Image(IconDir+IconSize+'/go_home.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap()
	png_cal = wx.Image(IconDir+IconSize+'/calendar.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap()
	png_cfg = wx.Image(IconDir+IconSize+'/settings.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap()
	png_forward = wx.Image(IconDir+IconSize+'/go_forward.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap()
	png_back = wx.Image(IconDir+IconSize+'/go_back.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap()
	png_imgbrowser = wx.Image(IconDir+IconSize+'/image_browser.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap()

	toolbar = self.CreateToolBar(wx.TB_HORIZONTAL | wx.TB_TEXT)
        toolbar.AddSimpleTool(100, png_save, "Save page")
        toolbar.AddSimpleTool(200, png_home, "Go home")
	toolbar.AddSimpleTool(220, png_back, "Go back")
	toolbar.AddSimpleTool(230, png_forward, "Go Forward")
        toolbar.AddSimpleTool(403, png_cal, "Calendar")
        toolbar.AddSimpleTool(400, png_cfg, "Settings")
	toolbar.AddSimpleTool(500, png_imgbrowser, "Image browser")
        toolbar.Realize()

    def RunScript(self, text, code):
	exec(code)
	return text

    def ShowAboutDlg(self, ev):
	pos = self.GetPosition()
        dialog = AboutDialog.Dialog(self, -1, 'About', '')
        dialog.SetPosition((pos[0]+100, pos[1]+100))

    def LoadDocumentation(self, ev):
	self.splitter.pr.html.LoadUrl('http://wixi.sourceforge.net/doc/v1.05/Home.html')

    def LinkedPages(self, ev):
	pages = sql.FindLinks(self.page_data.get('page'))
	self.splitter.pr.GenHtmlCode(pages)
	self.splitter.pr.SetSelection(0)

    def ShowCalendarDlg(self, ev):
	pos = self.GetPosition()
        dialog = CalendarDialog.Dialog(self, -1, 'Calendar', '')
        dialog.SetPosition((pos[0]+100, pos[1]+100))

    def QuickRef(self, event):
	pos = self.GetPosition()
        dialog = QuickrefDialog.Dialog(self, -1, 'Quick Reference')
        dialog.SetPosition((pos[0]+100, pos[1]+100))


    def StyleSheet(self, ev):

        if not sql.PageExist("stylesheet"):
    	    self.SetPage(None, "stylesheet")
	    self.splitter.pl.tree.Reset()
		
	    self.splitter.pr.edit.SetText(stylesheet)
	    self.splitter.pr.GenHtmlCode(stylesheet)
        else:
    	    dlg = wx.MessageDialog(self,'stylesheet already created!','New Page', wx.ICON_INFORMATION)
            dlg.ShowModal()
            dlg.Destroy()

    def ImageManager(self, ev):

	dlg = imagebrowser.ImageDialog(self, os.getcwd() )
        dlg.SetPosition(wx.GetMousePosition())

	if dlg.ShowModal() == wx.ID_OK:
	    file = dlg.GetFile()

	    if os.path.dirname(file) != self.sl3dir+'images':
		dest_file = self.sl3dir+'images/'+os.path.basename(file)

		if os.path.isfile(dest_file):
		    dlg = wx.MessageDialog(self,'Image %s already exist in Image map!' % (os.path.basename(file)),
			                        'Cannot copy file', wx.ICON_INFORMATION)
		    dlg.ShowModal()
		    dlg.Destroy()
		else:
		    shutil.copyfile(file, dest_file)
	    
	    self.splitter.pr.edit.AddText('['+os.path.basename(file)+']')
	    self.splitter.pr.edit.AppendText("%!wixi-images: "+os.path.basename(file)+"(#|#)\n")
	    self.splitter.pr.SetSelection(1)
	
	dlg.Destroy()

    def OnSearchDClick(self, ev):
	highlight = self.splitter.pl.search.findtext.GetValue()
	pg_data = self.splitter.pl.search.GetSelection()
	pg_id = pg_data[0]
	pg_name = pg_data[1]

	self.SetPage(pg_id, pg_name, highlight)

    def OnHistoryClick(self, ev):
	pg_id = self.splitter.pr.history.GetSelection()
	pg_id = pg_id[0]

	self.SetPage(pg_id, self.page_data.get('page'))

    def GoHome(self, event):

	pg_id = sql.GetPageId('Home')
	self.SetPage(pg_id, 'Home')

    def GoBack(self, event):
	'''Go one page back in the history buffer'''

	if self.splitter.pr.html.online_mode:
	    self.splitter.pr.html.HistoryBack()
	else:

	    backw = self.cursor -1
	    history = self.history[:]

	    if not backw < 0:

		pg_title = history[backw]

		if sql.PageExist(pg_title):
		    pg_id = sql.GetPageId(pg_title)
		    self.SetPage(pg_id, pg_title)
	    
	    else: backw += 1

	    self.cursor = backw
	    self.history = history[:]
	
    def GoForward(self, event):
	'''Go one page forward in the history buffer'''

	if self.splitter.pr.html.online_mode:
	    self.splitter.pr.html.HistoryForward()
	else:

	    forw = self.cursor +1
	    history = self.history[:]

	    if not forw > len(self.history)-1 :

		pg_title = history[forw]

		if sql.PageExist(pg_title):
		    pg_id = sql.GetPageId(pg_title)
		    self.SetPage(pg_id, pg_title)
	    
	    else: forw -= 1

	    self.cursor = forw
	    self.history = history[:]
	
    def ImportFiles(self, event):

        wildcard = "t2t Files (*.t2t)|*.t2t|txt Files (*.txt)|*.txt"

        dlg = wx.FileDialog( self, message="Choose files to import", defaultDir=self.sl3dir,
                             defaultFile="", wildcard=wildcard, style=wx.OPEN | wx.MULTIPLE | wx.CHANGE_DIR)

        if dlg.ShowModal() == wx.ID_OK:
            files = dlg.GetPaths()
            passed, failed, skip = 0, 0, 0
            for file in files:
                page = os.path.split(file)[-1][:-4]
                if sql.PageExist(page):
                    msg = wx.MessageDialog(self,'Page %s already exist.\nContinue importing this file?' % (page),'Import',wx.YES_NO | wx.ICON_INFORMATION)
                    if msg.ShowModal() == 5104: skip = 1
                    msg.Destroy()
                if skip != 1:
                    try:
                        contents = open(file)
                        contents = unicode(contents.read(), self.settings.ReadPickle().get('encoding'))
                        data = {'page':page, 'contents':contents,'comment':'Imported file'}
                        sql.SavePage(data)
                        passed += 1
                    except:
                        failed += 1
                skip = 0

            msg = wx.MessageDialog(self,'Imported %s file(s)\n%s failed.' % (passed, failed), 'Import',wx.OK|wx.ICON_INFORMATION)
            msg.ShowModal()
            msg.Destroy()

	    os.chdir(self.sl3dir+'images')

        dlg.Destroy()
        self.splitter.pl.tree.Reset()
    
    def DumpAllPages(self, ev):
	
	c = 0
	
	for page in sql.ShowAllPages():

	    pg_id = page.get('id')
	    pg_title = page.get('name')

	    data = sql.GetPageData(pg_id)
	    filesave = FormatGenerator.format(self, data.get('contents'))
	    filesave.Save(self.sl3db, pg_title)
	    
	    c += 1

        msg = wx.MessageDialog(self,'Exported %s pages.' % (c), 'Export',wx.OK|wx.ICON_INFORMATION)
        msg.ShowModal()
        msg.Destroy()

    def SavePage(self, event):
    
	pg_title = self.page_data.get('page')
	pg_id = self.page_data.get('id')
        contents = self.splitter.pr.edit.GetText()

        comment = self.splitter.pr.edit.comment.GetValue()
        self.splitter.pr.edit.comment.SetValue("")

        data = {}
        data.update( {'page': pg_title} )
        data.update( {'contents': contents} )
        data.update( {'comment': comment} )
        data.update( {'id': pg_id })

	if pg_title not in special_pages: sql.SavePage(data)

        filesave = FormatGenerator.format(self, contents)
        filesave.Save(self.sl3db, pg_title)

	pg_id = sql.GetPageId(pg_title)
	self.SetPageData(pg_id, pg_title, highlight=None)

	self.splitter.pl.tree.Reset()

    def SetPage(self, pg_id, pg_title, highlight=None):

	if self.splitter.pr.edit.modified > 1:
	    self.SavePage("ev")
	else:
	    self.SetPageData(pg_id, pg_title, highlight)

    def SetPageData(self, pg_id, pg_title, highlight):

	if not pg_id:
	    self.splitter.pr.edit.SetText("\n== %s ==\n" % (pg_title.replace("_"," ")))
	    self.splitter.pr.GenHtmlCode("\n== %s ==\n" % (pg_title.replace("_"," ")))
	    self.splitter.pr.history.SetList([])
	    self.page_data = {'page':pg_title, 'id':0}
	else:
            global page
	    data = sql.GetPageData(pg_id)
            page = data

	    self.page_data = data
	    hist = sql.GetPageHistory(data.get('page'))

	    self.splitter.pr.edit.SetText(data.get('contents'))
	    self.splitter.pr.GenHtmlCode(data.get('contents'), highlight)
	    self.splitter.pr.history.SetList(hist)
	    self.splitter.pr.compare.SetList(hist)

	if pg_title == "Changelog": self.splitter.pr.GenHtmlCode(sql.GenerateChangelog())
	if pg_title == "AllPages": self.splitter.pr.GenHtmlCode(sql.GenerateAllPages())

	self.splitter.pr.SetSelection(0)
	self.splitter.pr.html.html_mode = False
	
	if self.history[-1] != pg_title:
	    self.history = self.history[:self.cursor+1]
	    self.history.append(pg_title)
	if len(self.history) > 9:
	    self.history = self.history[-10:]
	self.cursor = len(self.history)-1

	self.splitter.pr.edit.modified = 0

    def NewPage(self, event):

        page = False
        dlg = wx.TextEntryDialog(self, 'Page name:', 'New Page')
        dlg.SetPosition(wx.GetMousePosition())
        if dlg.ShowModal() == wx.ID_OK: 
            page = dlg.GetValue()
            page = page.replace(' ','_')
        dlg.Destroy()

        if page:
            if not sql.PageExist(page):
                self.SetPage(None, page)
		self.splitter.pl.tree.Reset()
            else:
                dlg = wx.MessageDialog(self,'Page %s already exist!' % (page),'New Page', wx.ICON_INFORMATION)
                dlg.ShowModal()
                dlg.Destroy()

    def GetSelection(self):
        try:
            item = self.splitter.pl.tree.GetSelection()
            if self.splitter.pl.tree.GetItemText(item) != 'Index':
                return self.splitter.pl.tree.GetItemInfo(item)
        except:
            return None

    def OnLeftClick(self, event):
        item = self.GetSelection()
        if item != None:
            if item.get('type') == 'page':
		page_name = item.get('name')
		if page_name not in special_pages:
		    page_id = sql.GetPageId(page_name)
		    data = sql.GetPageData(page_id)
		else: page_id = 0

		self.SetPage(page_id, page_name)

    def DeleteItem(self, event):

        item = self.GetSelection()
        child = self.splitter.pl.tree.GetSelection()

        if item != None:
            if item.get('type') == 'page':

                pg_title = sql.GetPageTitle(item.get('id'))
                if sql.ValidPage(pg_title):
                    pg_id = sql.GetPageId(pg_title)
		    sql.DeletePage(pg_id)

                    self.splitter.pl.tree.RefreshDelete(child, 'page', pg_title)
                else: self.splitter.pl.tree.Reset()

            if item.get('type') == 'category':
                path = sql.GetPathName(item.get('id'))
                name = path.split('/')[-1]
                sql.DeletePath(item.get('id'))
                self.splitter.pl.tree.RefreshDelete(child, 'category', name)

    def RenameItem(self, event):

        item = self.GetSelection()
        child = self.splitter.pl.tree.GetSelection()

        if item != None:

            dlg = wx.TextEntryDialog(self,'New name:','Rename item', item.get('name'))
            dlg.SetPosition(wx.GetMousePosition())

            if dlg.ShowModal() == wx.ID_OK:
                newname = dlg.GetValue()
                dlg.Destroy()
            else: newname = ''

            if newname != item.get('name') and newname != '':

                if item.get('type') == 'page':
                    if not sql.PageExist(newname):
                        sql.RenamePage(item.get('id'), newname)
                        self.splitter.pl.tree.SetItemText(child, newname)
                        self.splitter.pl.tree.RefreshRename(item.get('type'), item.get('name'), newname)
                    else:
                        dlg = wx.MessageDialog(self,'Page %s already exist!' % (newname),'New Page', wx.ICON_INFORMATION)
                        dlg.ShowModal()
                        dlg.Destroy()
                else:
                    sql.RenamePath(item.get('path'), newname)
                    self.splitter.pl.tree.SetItemText(child, newname)
                    self.splitter.pl.tree.RefreshRename(item.get('type'), item.get('name'), newname) 

    def OnRightClick(self, event):

	def AddCategoryToText(ev):
	    self.splitter.pr.edit.AppendText("%!wixi-category: "+item.get('name')+"\n")
	    self.splitter.pr.SetSelection(1)

	def AddPageToText(ev):
	    if ' ' in item.get('name'): self.splitter.pr.edit.AddText("["+item.get('name').replace("_"," ")+' ""'+item.get('name')+'""]')
	    else: self.splitter.pr.edit.AddText("["+item.get('name').replace("_"," ")+" "+item.get('name')+"]")
	    self.splitter.pr.SetSelection(1)

        pt = event.GetPosition()
        item, flags = self.splitter.pl.tree.HitTest(pt)

        if self.options.get('navpanel') == "right":
            pt = (pt[0] + self.splitter.GetSashPosition(), pt[1])

        if item:
            menu = wx.Menu()
            item = self.GetSelection()
            if item != None and item.get('type') == 'page':
		if item.get('name') not in immutable_pages:
		    menu.Append(600, "rename page")
		    menu.Append(601, "delete page")
		menu.Append(602, "add to text")
		self.Bind(wx.EVT_MENU, self.RenameItem, id=600)
                self.Bind(wx.EVT_MENU, self.DeleteItem, id=601)
		self.Bind(wx.EVT_MENU, AddPageToText, id=602)
            else:
                menu.Append(600, "new category")
		menu.Append(601, "rename category")
                menu.Append(602, "remove category")
                menu.Append(603, "append to text")
                self.Bind(wx.EVT_MENU, self.NewCategory, id=600)
                self.Bind(wx.EVT_MENU, self.RenameItem, id=601)
                self.Bind(wx.EVT_MENU, self.DeleteItem, id=602)
		self.Bind(wx.EVT_MENU, AddCategoryToText, id=603)

            if '__WXGTK__' in wx.PlatformInfo: pt = (pt[0], pt[1]+30)
	    else: pt = (pt[0], pt[1]+50)

            self.PopupMenu(menu, pt)
            menu.Destroy()

    def NewCategory(self, event):

        item = self.GetSelection()
        child = self.splitter.pl.tree.GetSelection()

        path, category = '/', ''
        if item and item.get('type') == 'category':
            path = sql.GetPathName(item.get('id'))

        dlg = wx.TextEntryDialog(self, 'Category name:', 'In '+path)
        dlg.SetPosition(wx.GetMousePosition())
        if dlg.ShowModal() == wx.ID_OK:
            category = dlg.GetValue()
            category = category.replace(' ','_')
        if category:
            if path != '/': path += '/'
            cat_ev = sql.NewPath(path+category)
	    if cat_ev:
		self.splitter.pl.tree.RefreshAppend(child, cat_ev, path+category)

        dlg.Destroy()

    def ShowSettingsDlg(self, event):
	pos = self.GetPosition()
        dialog = SettingsDialog.SettingsDialog(self, -1, 'Settings', '')
        dialog.SetPosition((pos[0]+100, pos[1]+100))

    def CreateDirLayout(self):
	self.settings = SettingsDialog.Config()

	if not self.options.get('repository'): self.sl3dir = self.settings.ReadPickle().get('sl3dir') + '/'
	else: self.sl3dir = self.options.get('repository') + '/'
	
        try:
            if not os.path.isdir(self.sl3dir): os.mkdir(self.sl3dir)
            if not os.path.isdir(self.sl3dir+'images'): os.mkdir(self.sl3dir+'images')
            if not os.path.isdir(self.sl3dir+'plugins'): os.mkdir(self.sl3dir+'plugins')
	    if not os.path.isdir(self.sl3dir+'export'): os.mkdir(self.sl3dir+'export')
        except:
            print "Could not create directory layout"
        
        try: os.chdir(self.sl3dir+'images')
        except: print "Could not change to image directory"

    def GetLastSl3(self):
	cfg = SettingsDialog.Config()
	lastsl3 = cfg.ReadPickle()
	lastsl3 = lastsl3.get('lastopened')

	if lastsl3 in self.GetSl3Files(): return lastsl3
	else: return None

    def SetLastSl3(self, sl3file):
	cfg = SettingsDialog.Config()
	values = cfg.ReadPickle()
	values.update({'lastopened':sl3file})
	cfg.WritePickle(values)

    def GetSl3Files(self):

        files = []
        for file in os.listdir(self.sl3dir):
            if file[-4:] == '.sl3':
                files.append(file[:-4])
        return files

    def Sl3Connect(self, sl3file=None, new=False):

	global sql, immutable_pages, special_pages, special_categories
	
	self.sl3db = sl3file
	immutable_pages = ('Home','Changelog','AllPages')
	special_pages = ('Changelog','AllPages')

        if not sl3file:
	    lastsl3 = self.GetLastSl3()
	    sl3files = self.GetSl3Files()
            if lastsl3:
                sl3db = self.sl3dir + lastsl3 + '.sl3'
                self.Sl3Connect(sl3db)
	    elif sl3files:
		sl3db = self.sl3dir + sl3files[0] + '.sl3'
		self.Sl3Connect(sl3db)
	    else:
                startup = True
                self.NewSl3File('ev', startup)
        elif new:
            self.sql = SQLiteCon.sql(sl3file, new)
	    sql = self.sql

            self.splitter.pl.tree.AddIndex()
            self.SetStatusText(sl3file)
	    self.history = ['Home']
	    self.GoHome('ev')
        else:
	    if os.path.isfile(sl3file): self.sql = SQLiteCon.sql(sl3file)
	    elif os.path.isfile(self.sl3dir +sl3file): self.sql = SQLiteCon.sql(self.sl3dir + sl3file)
	    elif os.path.isfile(self.sl3dir +sl3file +'.sl3'): self.sql = SQLiteCon.sql(self.sl3dir + sl3file + '.sl3')
	    else: print 'Error: no valid database file found!'

	    sql = self.sql
	    
	    sqlver = sql.GetVersion()
	    if sqlver != "1.01":
	        sql.Disconnect()
	        convert = SQLiteCon.Conversion(sl3file, sqlver)
	        self.Sl3Connect(sl3file)

            self.splitter.pl.tree.AddIndex()
            self.SetStatusText(sl3file)
            self.history = ['Home']
	    self.GoHome('ev')

    def NewSl3File(self, event, startup=False):
    
        dlg = wx.TextEntryDialog(self,'Create a new wiki database:','Wixi', '')
        dlg.SetPosition(wx.GetMousePosition())

        new = True
        if dlg.ShowModal() == wx.ID_OK:
            
	    name = dlg.GetValue()
            if len(name) == 0: name = 'unnamed'

            sl3file = self.sl3dir + name + '.sl3'
            if not os.path.isfile(sl3file):
                self.Sl3Connect(sl3file, new)
		self.SetLastSl3(name)
                dlg.Destroy()
        else:
            dlg.Destroy()
            if startup:
                self.Close()

    def OpenSl3File(self, event):

        sl3files = self.GetSl3Files()

        if sl3files:

            dlg = wx.SingleChoiceDialog(self,"select a database","sqlite files", sl3files)
            dlg.SetPosition(wx.GetMousePosition())
            if (dlg.ShowModal() == wx.ID_OK):
                sl3file = self.sl3dir + dlg.GetStringSelection() + '.sl3'
                sql.Disconnect()
                self.Sl3Connect(sl3file)
		self.SetLastSl3(dlg.GetStringSelection())

		self.History = ['Home']
		self.GoHome('ev')

            dlg.Destroy()
            
    def OnClose(self, ev):
	
	if self.splitter.pr.edit.modified > 1:
    	    self.SavePage("ev")
    	    sql.Disconnect()
            self.SaveWindowSettings()
            self.Destroy()
        
        else:
	    sql.Disconnect()
	    self.SaveWindowSettings()
	    self.Destroy()


class ViewFrame(MainFrame):

    def LoadMenu(self):

        MenuWiki = wx.Menu()                                                                                                            
	MenuWiki.Append(102, "Open wiki", "Open a wiki database file")
        MenuWiki.AppendSeparator()
	MenuWiki.Append(105, "Exit", "Exit program")                                                                                           
	MenuHelp = wx.Menu()
	MenuHelp.Append(303, "About","About dialog")

        menuBar = wx.MenuBar()
        menuBar.Append(MenuWiki,"&Wiki")
	menuBar.Append(MenuHelp, "&Help")
	
	self.SetMenuBar(menuBar)

    def LoadIcons(self):

	IconSize = "16x16"
	if '__WXGTK__' in wx.PlatformInfo: IconSize = self.options.get('iconsize')

	png_home = wx.Image(IconDir+IconSize+'/go_home.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap()
	png_forward = wx.Image(IconDir+IconSize+'/go_forward.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap()
	png_back = wx.Image(IconDir+IconSize+'/go_back.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap()

	toolbar = self.CreateToolBar(wx.TB_HORIZONTAL | wx.TB_TEXT)
        toolbar.AddSimpleTool(200, png_home, "Go home")
	toolbar.AddSimpleTool(220, png_back, "Go back")
	toolbar.AddSimpleTool(230, png_forward, "Go Forward")
        toolbar.Realize()
        
    def OnRightClick(self, event):
        viewmode = True

    def CreateDirLayout(self):
	self.settings = SettingsDialog.Config()

	if not self.options.get('repository'): self.sl3dir = self.settings.ReadPickle().get('sl3dir') + '/'
	else: self.sl3dir = self.options.get('repository') + '/'
        
        try: os.chdir(self.sl3dir+'images')
        except: False

if __name__ == "__main__":

    app = wx.PySimpleApp()
    main = MainFrame(None, -1, "Wixi ~~testing~~")
    app.MainLoop()


