
from distutils.core import setup

setup(scripts=['script/wixi'],
      name='Wixi',
      version='1.05',
      description='Multi-platform wiki application for the desktop',
      author='Kees Remmelzwaal',
      author_email='k.remmelzwaal@planet.nl',
      url='http://wixi.sourceforge.net',
      license = 'GPL',
      packages=['wixi'],
      package_data={'wixi':['icons/16x16/*','icons/22x22/*']}
)

