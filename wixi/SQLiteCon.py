
#   Copyright (C) 2007 k.remmelzwaal@planet.nl
#
#   This program is free software; you can redistribute it and/or
#   modify it under the terms of the GNU General Public License
#   as published by the Free Software Foundation; either version 2
#   of the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


# backwards compatible with python <= 2.4
try:
    import sqlite3 as sqlite
except:
    from pysqlite2 import dbapi2 as sqlite

import os
import re

Home = '''

== Welcome ==
This is the Home page and can be used for indexing your pages.

**Getting started**
- Go to the editor tab to modify this page.
- The quick reference will show you how to use the markup. 
- Go to the help menu to load updated online documentation.

'''

SqlScript = '''
create table pages(
  pg_id INTEGER PRIMARY KEY,
  pg_title TEXT,
  pg_text TEXT,
  pg_comment TEXT,
  pg_removed INTEGER,
  pg_date TEXT
);
create table categorylist(
  cg_id INTEGER,
  cg_title TEXT,
  cg_category TEXT
);
create table categorytree(
  tr_id INTEGER PRIMARY KEY,
  tr_depth INTEGER,
  tr_path TEXT
);
create table version(
  db_version INTEGER
);

insert into pages values( null, "Home","%s","Created index page", 0, datetime("now","localtime") );

insert into categorytree values( null, 1, "/Events");
insert into categorytree values( null, 2, "/Events/RemovedPages");
insert into categorytree values( null, 2, "/Events/AllPages");

insert into version values("1.01");

''' % (Home)


########## Categories SQL class ####################################################################

class categories:

    def DeletePath(self, ev_cat):

        path = self.GetPathName(ev_cat)

        paths = []
        paths.append(path)

        self.cur.execute('select tr_path from categorytree where tr_path like "%s" order by tr_path' % (path+'/%'))
        fetch = self.cur.fetchall()
        
        for p in fetch:
            paths.append(p[0])

        for p in paths:
            self.cur.execute('delete from categorytree where tr_path="%s" ' % (path))
        self.con.commit()


    def RenamePath(self, path, new_name):

        tree = path + "/%"
        self.cur.execute('select tr_path from categorytree where tr_path like "%s" order by tr_path' % (tree))
        fetch = [p[0] for p in self.cur.fetchall()]
        fetch = [path] + fetch

        for old_path in fetch:
            old_name = path.split('/')[-1]
            new_path = old_path.replace(old_name, new_name, 1)

            self.cur.execute('update categorytree set tr_path="%s" where tr_path="%s" ' % (new_path, old_path))

	self.con.commit()

    def NewPath(self, path):

        self.cur.execute('select tr_path from categorytree where tr_path="%s" ' % (path))
        if self.cur.fetchall() == []:

            depth = len(path.split('/'))-1
            self.cur.execute('insert into categorytree values(null,"%s","%s")' % (depth, path))
            self.con.commit()

            self.cur.execute('select tr_id from categorytree where tr_path="%s"' % (path))
            fetch = self.cur.fetchone()

            return fetch
        else: 
            return False


    def GetCategoryNames(self, path):

        if path == '/': depth = 1
        else: depth = len(path.split('/'))
        tree = path + "%"

        self.cur.execute('select * from categorytree where tr_depth="%s" and tr_path like "%s" order by tr_path' % (depth, tree))
        fetch = [i for i in self.cur.fetchall()]

        category_names = []
        for cid, depth, path in fetch:
            if path != tree[:-1]:
                name = path.split('/')[-1]
                category_names.append( {'type':'category','id':cid,'name':name,'path':path} )

        return category_names

    def GetPathName(self, cat_id):
        self.cur.execute('select tr_path from categorytree where tr_id="%s"' % (cat_id))
        cat = self.cur.fetchone()[0]
        return cat

    def ShowPathList(self):
        self.cur.execute('select tr_path from categorytree order by tr_path')
        list = [i[0] for i in self.cur.fetchall()]
        return list


########## Pages SQL Class #########################################################################

class pages:

    def DeletePage(self, page_id):
        self.cur.execute('select pg_title from pages where pg_id=%s' % (page_id))
        title = self.cur.fetchone()[0]

	c = 0
	while 1:
	    new_title = title + '.' + str(c)
	    if not self.PageExist(new_title): break
	    else:
		c+=1
		continue

        self.cur.execute('update pages set pg_title="%s", pg_removed="1" where pg_title="%s" ' % (new_title, title))
        self.cur.execute('delete from categorylist where cg_title="%s" ' % (title))
        self.con.commit()

    def ValidPage(self, title):
        self.cur.execute('select pg_id from pages where pg_title="%s" and pg_removed=0' % (title))
        fetch = self.cur.fetchone()

        if fetch == None: return False
        else: return True

    def PageExist(self, title):
        self.cur.execute('select pg_id from pages where pg_title like "%s"' % (title))
        e = self.cur.fetchall()
        e = len(e)

        if e > 0: return True
        else: return False

    def SavePage(self, data):

	comment = data.get("comment")    
        contents = data.get('contents')
        contents = contents.replace("'","''")

        if len(comment) == 0 and data.get('id') == 0:
    	    comment = "Created page %s." % (data.get('page'))
        
        if len(data.get("comment")) == 0 and data.get('id') != 0:
    	    self.cur.execute("update pages set pg_text='%s' where pg_id='%s' " % (contents, data.get('id')))
    	else:
    	    self.cur.execute('update pages set pg_removed="1" where pg_title="%s" ' % (data.get('page')) )
    	    self.cur.execute("insert into pages values(null, '%s', '%s', '%s', 0, datetime('now','localtime'))" % (data.get('page'), contents ,comment))
        self.con.commit()

        self.cur.execute('select pg_id from pages order by pg_id desc limit 1')
        data.update({'id': self.cur.fetchone()[0] })

        self.SaveCategory(data)

    def GetPageData(self, page_id):
        self.cur.execute('select * from pages where pg_id="%s" ' % (page_id))
        fetch = self.cur.fetchall()[0]

        data = {}
        data.update( {'id': fetch[0]} )
        data.update( {'page': fetch[1] }   )
	data.update( {'title': fetch[1] } )
        data.update( {'contents': fetch[2] } )
        data.update( {'comment': fetch[3] }   )
        data.update( {'date': fetch[5] }     )

        return data

    def RenamePage(self, page_id, new_name):
        self.cur.execute('select pg_title from pages where pg_id=%s limit 1' % (page_id))
        title = self.cur.fetchone()[0]

        self.cur.execute('update pages set pg_title="%s" where pg_title="%s" ' % (new_name, title))
	self.cur.execute('update categorylist set cg_title="%s" where cg_title="%s" ' % (new_name, title))
        self.con.commit()

    def ShowRemovedPages(self):
        self.cur.execute('select pg_id, pg_title from pages where pg_removed=1 order by pg_date desc')
        pages = self.cur.fetchall()

        deleted, removed_pages = [], []
        for pg_id, pg_title in pages:
	    if not self.ValidPage(pg_title):
		if pg_title not in deleted:
		    removed_pages.append( {'type':'page','id': pg_id,'name':pg_title} )
		    deleted.append(pg_title)
        return removed_pages

    def GetPageTitle(self, page_id):
        self.cur.execute('select pg_title from pages where pg_id=%s' % (page_id))
        title = self.cur.fetchone()[0]
        return title

    def GetPageId(self, title):
        self.cur.execute('select pg_id from pages where pg_title="%s" order by pg_id desc limit 1' % (title))
        page_id = self.cur.fetchone()[0]
        return page_id

    def GetPageNames(self, category):
        self.cur.execute('select cg_id, cg_title from categorylist where cg_category="%s" order by cg_title' % (category))
        fetch = self.cur.fetchall()

        pages = []
        for pgid, pgname in fetch:
            pages.append( {'type':'page','id': pgid,'name':pgname} )
        return pages

    def ShowAllPages(self):
        self.cur.execute('select pg_id, pg_title from pages where pg_removed=0 order by pg_title')
        fetch = self.cur.fetchall()

        pages = []
        for page_id, page_name in fetch:
            pages.append( {'type':'page','id': page_id,'name':page_name} )
        return pages

    def GetPageHistory(self, title):
        self.cur.execute('select pg_id, pg_date, pg_comment from pages where pg_title="%s" order by pg_id desc' % (title))
        return self.cur.fetchall()


########## Misc SQL Class  #########################################################################


class sql(categories, pages):

    def __init__(self, sl3db, new=False):

        sl3dir = os.path.dirname(sl3db)

        if not new:
            self.con = sqlite.connect(sl3db)
            self.cur = self.con.cursor()
        else:
            self.con = sqlite.connect(sl3db)
            self.cur = self.con.cursor()
            self.cur.executescript(SqlScript)

    def Disconnect(self):
        self.con.close()

    def SaveCategory(self, data):

        def FindCategories(text):
            '''It is allowed to use spaces between the categories and comma. 
            Spaces in category names are also allowed but not recommend.'''

	    patt_cattag = re.compile("^%!wixi-category:.*?$", re.MULTILINE) # find wixi-category tags
            patt_split = re.compile("\s*,\s*") # splitting by comma
            patt_rmspaces = re.compile("\w+") # getting the whole words without spaces
            
            fetch = patt_cattag.findall(text)

	    categories = []
            for cats in fetch:
            
                # stripping out the wixi-category tag
		if cats[-1:] == "\r": cats = cats[16:-1]
		else: cats = cats[16:]

                cats = patt_split.split(cats) # splitting categories by comma's

		for cat in cats:
		
		    c = patt_rmspaces.findall(cat) # filtering all spaces.

		    cat = ""
		    for i in c: cat += i + " " # rebuilding correct category name.
		    cat = cat[:-1]
		    
		    if cat not in categories and len(cat) != 0: # only append if not add earlier and if not empty.
			categories.append(cat)

            ### compatibility mode ###

            old_pattern = re.compile("category\[.*?\]", re.MULTILINE)
            old_fetch = old_pattern.findall(text)

	    for cat in old_fetch:
	        if cat not in categories:
	            categories.append(cat[9:-1])
                    print "*** WARNING: THIS PAGE CONTAINS OLD CATEGORY MARKS! ***" # display a warning

            ### compatibility mode ###

	    return categories

        categories = FindCategories(data['contents'])
        self.cur.execute('delete from categorylist where cg_title="%s" ' % (data['page']))

	for category in categories:
	    self.cur.execute('insert into categorylist values("%s","%s","%s")' % (data['id'], data['page'], category))
        
	self.con.commit()

    def GetCategoryEvent(self, cat):
        self.cur.execute('select tr_id from categorytree where tr_path="%s"' % (cat))
        ev_cat = self.cur.fetchone()[0]
        return ev_cat

    def DeleteHistory(self):
        self.cur.execute('delete from pages where pg_removed=1')
        self.cur.execute('VACUUM')
        self.con.commit()

    def FindLinks(self, title):
        find = '% ' + title + ']%'
        self.cur.execute('select pg_id, pg_title from pages where pg_text like "%s" and pg_removed=0 ' % (find))
        pages = self.cur.fetchall()

	contents = "\n- **"+title+"**\n"
	for page in pages:
	    contents += " - ["+page[1].replace("_"," ")+" "+page[1]+"]\n"

	return contents

    def Search(self, text):
        text = '%' + text + '%'
        self.cur.execute('select pg_id, pg_title from pages where pg_title like "%s" and pg_removed=0\
                          or pg_text like "%s" and pg_removed=0' % (text, text))
        return self.cur.fetchall()

    def GenerateAllPages(self):
	self.cur.execute('select pg_id, pg_title from pages where pg_removed=0 order by pg_title')
	pages = self.cur.fetchall()

	contents = "\n=== All Pages ===\n\n"

	for page in pages:
	    contents += "- ["+page[1]+' ""'+page[1]+'""]\n'

	return contents


    def GenerateChangelog(self):
        self.cur.execute('select pg_id, pg_date, pg_title, pg_comment from pages order by pg_date desc limit 60')
	pages = self.cur.fetchall()
        
	contents = "\n=== Changelog ===\n\n"
	contents += "|| **date** | **title** | **comment** |\n"
	
	for page in pages:
	    contents += "| "+ page[1]+" | ["+page[2]+' ""'+page[2]+'""] | '+page[3]+" |\n"
	
	return contents


    def GetVersion(self):
        try:
            self.cur.execute('select * from version')
            return str(self.cur.fetchone()[0])
        except:
            return None

class Conversion:

    def __init__(self, sl3file, version):

        os.rename(sl3file, sl3file+".old")
        self.con = sqlite.connect(sl3file+".old")
        self.cur = self.con.cursor()
        self.newsql = sql(sl3file, new=True)

        if version == None: self.From_076()
        if version == "0.81": self.From_081()

        self.con.close()

    def From_076(self):

        self.cur.execute('select id from pages where absolete="0"')
        pages = self.cur.fetchall()
        
        self.cur.execute('select category from cattree')
        categories = self.cur.fetchall()
        
        for page_id in pages:
            self.cur.execute('select page, contents, summary from pages where id="%s"' % (page_id))
            data = self.cur.fetchall()[0]
            data = {'page':data[0],'contents':data[1],'comment':data[2]}
            self.newsql.SavePage(data)
        
        for cat in categories:
            self.newsql.NewPath(cat[0])

    def From_081(self):

        self.cur.execute('select id from pages where absolete="0"')
        pages = self.cur.fetchall()
        
        self.cur.execute('select path from cattree')
        categories = self.cur.fetchall()
        
        for page_id in pages:
            self.cur.execute('select page, contents, comment from pages where id="%s"' % (page_id))
            data = self.cur.fetchall()[0]
            data = {'page':data[0],'contents':data[1],'comment':data[2]}
            self.newsql.SavePage(data)
        
        for cat in categories:
            self.newsql.NewPath(cat[0])


if __name__ == "__main__" :

    sql_db = sql('test.db',new=False)

    print sql_db.ShowChangelog()

    sql_db.Disconnect()


    



