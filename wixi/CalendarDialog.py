
#   Copyright (C) 2007 k.remmelzwaal@planet.nl
#
#   This program is free software; you can redistribute it and/or
#   modify it under the terms of the GNU General Public License
#   as published by the Free Software Foundation; either version 2
#   of the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import wx
import wx.lib.calendar
import calendar

class Dialog(wx.Dialog):

    def __init__(self, parent, id, title, size=wx.DefaultSize,
                 pos=wx.DefaultPosition, style=wx.DEFAULT_DIALOG_STYLE ):

	global sql
	sql = parent.sql
	self.parent = parent

        wx.Dialog.__init__(self, parent, id, title, size=(250,250))

        self.panel = wx.Panel(self)

        self.calend = wx.lib.calendar.Calendar(self.panel, -1)
        self.calend.SetColor('highlight_background', '#6699FF')
        self.calend.SetColor('selection_background', '#6699FF')
        self.calend.SetColor('selection_font', 'black')

        start_month = self.calend.GetMonth()
        start_year = self.calend.GetYear()

        monthlist = self.GetMonthList()
        self.date = wx.ComboBox(self.panel, -1, "", (1,1), (120,-1), monthlist,  wx.CB_DROPDOWN)
        self.date.SetSelection(start_month-1)

        self.dtext = wx.TextCtrl(self, -1, str(start_year), (200, 20), (60, -1))

        self.spin = wx.SpinButton(self.panel, -1, (270, 20), (20,10))
        self.spin.SetRange(1980, 2020)
        self.spin.SetValue(start_year)

        self.ResetDisplay()

        box1 = wx.BoxSizer(wx.HORIZONTAL)
        box1.Add(self.date, 5, wx.EXPAND)
        box1.Add(self.dtext, 3, wx.EXPAND)
        box1.Add(self.spin, 2, wx.EXPAND)

        box2 = wx.BoxSizer(wx.VERTICAL)
        box2.Add(box1, 1)
        box2.Add(self.calend, 10, wx.EXPAND)

        self.panel.SetSizer(box2)

        box = wx.BoxSizer(wx.VERTICAL)
        box.Add(self.panel, 1, wx.EXPAND)

        self.Bind(wx.lib.calendar.EVT_CALENDAR, self.MouseClick, self.calend)
        self.Bind(wx.EVT_COMBOBOX, self.EvtComboBox, self.date)
        self.Bind(wx.EVT_SPIN, self.OnSpin, self.spin)

        self.SetSizer(box)
        self.SetAutoLayout(1)

        self.Show(1)

    def MouseClick(self, evt):
        if evt.click == "DLEFT":
            day = str('%02d' % (evt.day))
            month = str('%02d' % (evt.month))
            year = str(evt.year)

            page = year+'-'+month+'-'+day

            if sql.PageExist(page):
                page_id = sql.GetPageId(page)
                data = sql.GetPageData(page_id)
		self.parent.SetPage(data.get('id'), page)
	    else:
		self.parent.SetPage(None, page)

    def ResetDisplay(self):

        month = self.calend.GetMonth()
        year = self.calend.GetYear()

        end_month_day = calendar.monthrange(year, month)[1]

        set_days = []
        for day in range(1, end_month_day+1):
            page = str(year)+'-'+str('%02d'%(month))+'-'+str('%02d'%(day))
            if sql.PageExist(page):
                set_days.append(day)

        self.calend.SetSelDay(set_days)
        self.calend.Refresh()

    def OnSpin(self, event):
        year = event.GetPosition()
        self.dtext.SetValue(str(year))
        self.calend.SetYear(year)
        self.ResetDisplay()

    def EvtComboBox(self, event):
        name = event.GetString()
        monthval = self.date.FindString(name)
        self.calend.SetMonth(monthval+1)
        self.ResetDisplay()

    def GetMonthList(self):

        monthlist = []
        for i in range(13):
            name = wx.lib.calendar.Month[i]
            if name != None:
                monthlist.append(name)

        return monthlist


class TestFrame(wx.Frame):

    def __init__(self, parent, id, title):
        wx.Frame.__init__(self, parent, id, title, size=(300,150))
        dialog = CalendarDialog(self, -1, 'Calendar')
	self.Show(1)

if __name__ == "__main__":

    app = wx.PySimpleApp()
    main = TestFrame(None, -1, "Calendar")
    app.MainLoop()

