#!/usr/bin/python

#   Copyright (C) 2007 k.remmelzwaal@planet.nl
#
#   This program is free software; you can redistribute it and/or
#   modify it under the terms of the GNU General Public License
#   as published by the Free Software Foundation; either version 2
#   of the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import wx, sys, os, getopt
import wixi

version = '1.05'
showhelp = '''Wixi %s (c) 2006-2008 k.remmelzwaal@planet.nl
Options:
  -r <dir>  --repository=<dir>  (repository containing wiki's)
  -w <wiki> --wiki=<wiki>       (start with a specific wiki)
  -g <geom> --geometry=<geom>   (window width and height, default=850x550)
  -v        --viewmode          (view only mode)

  --navpanel=<left|off|right>   (disable or align category/search panel, default=left)
  --iconsize=<small|large>      (select the size for icons, default=large (gtk))
  ''' % (version)


def RunApp():

    #for path in sys.path:
    #	if path[-14:] == "/site-packages":
    #	    icondir = path + "/wixi/icons/"

    icondir = os.getcwd() + "/wixi/icons/"
    options.update({'icondir': icondir})


    if options.get('viewmode'):
        app = wx.PySimpleApp()
        main = wixi.MainFrame.ViewFrame(None, -1, "Wixi - " +version, options)
        app.MainLoop()    
    else:
        app = wx.PySimpleApp()
        main = wixi.MainFrame.MainFrame(None, -1, "Wixi - " +version, options)
        app.MainLoop()

if __name__ == '__main__':

    global options
    options, needhelp = {'navpanel':'left','iconsize':'22x22','viewmode':False}, False
    
    try:
	optlist, args = getopt.getopt(sys.argv[1:], 'r:w:g:v',['navpanel=','repository=','iconsize=','geometry=','wiki=','viewmode'])

	for opt, arg in optlist:

	    if opt in ("-r","--repository"): options.update({'repository': arg})
	    if opt in ("-v","--viewmode"): options.update({'viewmode': True})
	    if opt in ("-w","--wiki"): options.update({'wiki': arg})
	    if opt in ("-g","--geometry"):
		width = int(arg.split('x')[0])
		height = int(arg.split('x')[1])
		options.update({'geometry': (width,height)})
	    if opt == "--navpanel": options.update({'navpanel':arg})
	    if opt == "--iconsize":
		if arg == "small": options.update({'iconsize':'16x16'})
		else: options.update({'iconsize':'22x22'})
    
    except: needhelp = True

    if needhelp: print showhelp 
    else: RunApp()


