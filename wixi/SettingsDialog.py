
#   Copyright (C) 2007 k.remmelzwaal@planet.nl
#
#   This program is free software; you can redistribute it and/or
#   modify it under the terms of the GNU General Public License
#   as published by the Free Software Foundation; either version 2
#   of the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


import wx, pickle, os

startdir = os.getcwd()

class Config:

    def __init__(self):

        self.file = startdir +'/wixi.cfg'
        if os.name == 'posix':
            home = os.getenv('HOME')
            self.file = home+'/.wixi.cfg'

        if not os.path.exists(self.file):
            self.WritePickle()

    def WritePickle(self, config=None):

        if not config:
            config = self.DefaultValues()

        pkl_file = open(self.file, 'wb')
        config = pickle.dump(config, pkl_file)
        pkl_file.close()

    def ReadPickle(self):
        pkl_file = open(self.file, 'rb')
        config = pickle.load(pkl_file)
        pkl_file.close()
        return config

    def DefaultValues(self):

        config = {}

        if os.name == 'posix':
            home = os.getenv('HOME')
            config.update( {'sl3dir': home+'/MyWiki'} )
        else:
            config.update( {'sl3dir': startdir+'\MyWiki'} )

        config.update( {'encoding':'utf-8'} )
        config.update( {'export':['t2t']} )
	config.update( {'editfontsize':12} )
	config.update( {'navfontsize':10} )
	config.update( {'htmlfontsize':12} )
	config.update( {'lastopened':None} )
	config.update( {'editwrap': 0 })
	config.update( {'winset':((1000,600),200)} )

        return config


class SettingsDialog(wx.Dialog):

    def __init__(self, parent, id, title, size=wx.DefaultSize,
                 pos=wx.DefaultPosition, style=wx.DEFAULT_DIALOG_STYLE):

	global IconDir, sql
	self.parent = parent

	IconDir = parent.IconDir
	sql = parent.sql

        dlgsize=(470,320)
        if '__WXMAC__' in wx.PlatformInfo: dlgsize=(560,400)
        if '__WXGTK__' in wx.PlatformInfo: dlgsize=(560,370)

        wx.Dialog.__init__(self, parent, id, title, size=dlgsize)

        self.formats = ['t2t','html','xhtml','man','sgml','tex','mgp','lout','pm6','moin']
        self.lb = wx.CheckListBox(self, -1, (50, 50), wx.DefaultSize, self.formats)
        self.lb.SetSelection(0)

        self.path = wx.TextCtrl(self, -1, "", size=(250,-1))
        self.encoding = wx.TextCtrl(self, -1, "", size=(80,-1))
	self.editfontsize = wx.SpinCtrl(self, -1, "", (30,50))
	self.editfontsize.SetRange(5,15)
	self.editfontsize.SetValue(12)

	self.navfontsize = wx.SpinCtrl(self, -1, "", (30,50))
	self.navfontsize.SetRange(5,15)
	self.navfontsize.SetValue(12)

	self.htmlfontsize = wx.SpinCtrl(self, -1, "", (30,50))
	self.htmlfontsize.SetRange(5,22)
	self.htmlfontsize.SetValue(14)

	self.editwrap = wx.CheckBox(self, -1, "")

	png_dir = wx.Image(IconDir+'16x16/dir.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap()
	png_file = wx.Image(IconDir+'16x16/file.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap()

        self.HistdelBtn = wx.Button(self, 20, "Delete history", size=(150,30))
        self.OkBtn = wx.Button(self, 10, "Ok", size=(70,30))
        self.GetDirBtn = wx.BitmapButton(self, 30 , png_dir, (20, 20), (png_dir.GetWidth()+10, png_dir.GetHeight()+10))

        box2 = wx.BoxSizer(wx.VERTICAL)
        box2.Add(wx.StaticText(self, -1, "Export formats"))
        box2.Add(self.lb)

        box4 = wx.FlexGridSizer(6, 3, 10, 1) # row, cols
        box4.Add(wx.StaticText(self, -1, "Main repository :"), flag=wx.ALIGN_RIGHT)
        box4.Add(self.path)
        box4.Add(self.GetDirBtn)

	box4.Add(wx.StaticText(self, -1, "Editor font size :"), flag=wx.ALIGN_RIGHT)
	box4.Add(self.editfontsize)
	box4.Add((20,20))

	box4.Add(wx.StaticText(self, -1, "Categories font size :"), flag=wx.ALIGN_RIGHT)
	box4.Add(self.navfontsize)
	box4.Add((20,20))

	box4.Add(wx.StaticText(self, -1, "Html font size :"), flag=wx.ALIGN_RIGHT)
	box4.Add(self.htmlfontsize)
	box4.Add((20,20))

	box4.Add(wx.StaticText(self, -1, "Editor text wrap :"), flag=wx.ALIGN_RIGHT)
	box4.Add(self.editwrap)
	box4.Add((20,20))

        box4.Add(wx.StaticText(self, -1, "Encoding type :"), flag=wx.ALIGN_RIGHT)
        box4.Add(self.encoding)

        box3 = wx.BoxSizer(wx.VERTICAL)
        box3.Add(box4)
        box3.Add((20,20))
        box3.Add(self.HistdelBtn)

        box1 = wx.GridBagSizer(2,2)
        box1.Add(box2, (1,1))
        box1.Add(box3, (1,3))

        box = wx.BoxSizer(wx.VERTICAL)
        box.Add(box1)
        box.Add(self.OkBtn, flag=wx.ALIGN_CENTER)

        self.Bind(wx.EVT_BUTTON, self.Save, self.OkBtn)
        self.Bind(wx.EVT_BUTTON, self.ChooseDir, self.GetDirBtn)
	self.Bind(wx.EVT_BUTTON, self.DeleteHistory, self.HistdelBtn)

        self.SetSizer(box)
        self.SetAutoLayout(1)

        self.Show(1)

        self.SetValues()

    def DeleteHistory(self, event):
        dlg = wx.MessageDialog(self,'Are you sure you want to \ndelete all history information?',
                                    'Delete history',wx.YES_NO | wx.ICON_INFORMATION)

        val = dlg.ShowModal()
        if val == 5103:
            sql.DeleteHistory()
            self.HistdelBtn.Disable()
        dlg.Destroy()

    def ChooseDir(self, event):
        dlg = wx.DirDialog(self, "Choose a directory:", style=wx.DD_DEFAULT_STYLE|wx.DD_NEW_DIR_BUTTON)
        if dlg.ShowModal() == wx.ID_OK:
            self.path.SetValue(dlg.GetPath())
        dlg.Destroy()

    def ChooseFile(self, event):
        dlg = wx.FileDialog(self, message="Choose a file", style=wx.OPEN | wx.CHANGE_DIR)
        if dlg.ShowModal() == wx.ID_OK:
            self.editor.SetValue(dlg.GetPath())
        dlg.Destroy()

    def Save(self, event):

        export = []
        if self.lb.IsChecked(1): export.append('html')
        if self.lb.IsChecked(2): export.append('xhtml')
        if self.lb.IsChecked(0): export.append('t2t')
        if self.lb.IsChecked(3): export.append('man')
        if self.lb.IsChecked(4): export.append('sgml')
        if self.lb.IsChecked(5): export.append('tex')
        if self.lb.IsChecked(6): export.append('mgp')
        if self.lb.IsChecked(7): export.append('lout')
        if self.lb.IsChecked(8): export.append('pm6')
        if self.lb.IsChecked(9): export.append('moin')

        sl3dir = self.path.GetValue()
        encoding = self.encoding.GetValue()
	editfontsize = self.editfontsize.GetValue()
	navfontsize = self.navfontsize.GetValue()
	htmlfontsize = self.htmlfontsize.GetValue()
	lastopened = self.cfg.ReadPickle().get('lastopened')
	winset = self.cfg.ReadPickle().get('winset')
	editwrap = self.editwrap.IsChecked()

	if editwrap: editwrap = 1
	else: editwrap = 0

	config = {'export':export,'sl3dir':sl3dir,'encoding':encoding,'lastopened':lastopened,'winset':winset,
	          'editfontsize':editfontsize,'navfontsize':navfontsize,'htmlfontsize':htmlfontsize,'editwrap':editwrap}
        self.cfg.WritePickle(config)

	self.parent.splitter.pr.edit.SetFontSize(editfontsize)
	self.parent.splitter.pr.edit.SetWrapMode()
	self.parent.splitter.pr.html.SetFontSize(htmlfontsize)
	
	self.parent.splitter.pl.tree.SetFont(self.parent.splitter.pl.tree.root)
	self.parent.splitter.pl.tree.Reset()

        self.Close()

    def SetValues(self):

        self.cfg = Config()
        values = self.cfg.ReadPickle()
        
        if not values.get('editfontsize') : values.update({'editfontsize':12})
        if not values.get('navfontsize') : values.update({'navfontsize':10})
        if not values.get('htmlfontsize') : values.update({'htmlfontsize':12})
        if not values.get('editwrap') : values.update({'editwrap':0})

        if 'html' in values.get('export'): self.lb.Check(1)
        if 'xhtml' in values.get('export'): self.lb.Check(2)
        if 't2t' in values.get('export'): self.lb.Check(0)
        if 'man' in values.get('export'): self.lb.Check(3)
        if 'sgml' in values.get('export'): self.lb.Check(4)
        if 'tex' in values.get('export'): self.lb.Check(5)
        if 'mgp' in values.get('export'): self.lb.Check(6)
        if 'lout' in values.get('export'): self.lb.Check(7)
        if 'pm6' in values.get('export'): self.lb.Check(8)
        if 'moin' in values.get('export'): self.lb.Check(9)

        self.path.SetValue(values.get('sl3dir'))
	self.editfontsize.SetValue(values.get('editfontsize'))
	self.navfontsize.SetValue(values.get('navfontsize'))
	self.htmlfontsize.SetValue(values.get('htmlfontsize'))
        self.encoding.SetValue(values.get('encoding'))
        
        if values.get('editwrap') == 1:self.editwrap.SetValue(True)

class TestFrame(wx.Frame):

    def __init__(self, parent, id, title):
        wx.Frame.__init__(self, parent, id, title, size=(300,150))
        dialog = SettingsDialog(self, -1, 'Settings')
	self.Show(1)

if __name__ == "__main__":

    app = wx.PySimpleApp()
    main = TestFrame(None, -1, "Settings")
    app.MainLoop()

