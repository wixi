
#   Copyright (C) 2007 k.remmelzwaal@planet.nl
#
#   This program is free software; you can redistribute it and/or
#   modify it under the terms of the GNU General Public License
#   as published by the Free Software Foundation; either version 2
#   of the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

html_page = '''
<h3>About Wixi</h3><br>
<i>Wixi is a multi-platform wiki application for personal use. <br>
It depends on the following projects: </i><br>
<br>
<i>http://txt2tags.sourceforge.net <small>(document generator)</small></i><br>
<i>http://www.python.org <small>(program language)</small></i><br>
<i>http://www.wxpython.org <small>(gui toolkit)</small></i><br>
<i>http://www.lila-center.info <small>(lila icons)</small></i><br>
<br>
<small>
<code>
<br>
   Copyright (C) 2006-2008 k.remmelzwaal@planet.nl
<br>
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
<br><br>
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
<br><br>
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
<br><br>
</code>
</small>

'''


import wx
import wx.html as html

class HtmlPanel(html.HtmlWindow):

    def __init__(self, parent, id):
        html.HtmlWindow.__init__(self, parent, id, style=wx.NO_FULL_REPAINT_ON_RESIZE)


class Dialog(wx.Dialog):

    def __init__(self, parent, id, title, size=wx.DefaultSize,
                 pos=wx.DefaultPosition, style=wx.DEFAULT_DIALOG_STYLE ):

	wx.Dialog.__init__(self, parent, id, title, size=(400,300))
        
        self.html = HtmlPanel(self, -1)
	self.html.SetPage(html_page)
	self.OkBtn = wx.Button(self, 10, "Ok", size=(80,30))

        box = wx.BoxSizer(wx.VERTICAL)
        box.Add(self.html, 1, wx.EXPAND)
	box.Add(self.OkBtn, flag=wx.ALIGN_CENTER)

	self.Bind(wx.EVT_BUTTON, self.CloseDlg, self.OkBtn)

        self.SetSizer(box)
        self.SetAutoLayout(1)

	self.Show(1)


    def CloseDlg(self, ev):
	self.Close()


